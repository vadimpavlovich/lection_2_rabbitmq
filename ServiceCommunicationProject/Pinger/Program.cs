﻿using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Models;
using RabbitMQ.Wrapper; 
using System;
using System.IO;
using System.Text;
using System.Timers;

namespace Pinger
{
    class Program
    {
        public static ListenQueue Listener { get; set; }
        public static WriteQueue Sender    { get; set; } 

        static void Main(string[] args)
        {
            Console.WriteLine("Pinger runned. Type Enter to exit");
            Init();
            Sender.SendMessageToQueue("ping");
            Console.ReadLine();
        }

        public static void Init() 
        {
            var config = ReadConfig();
            Listener = new ListenQueue(config, MessageReceived); 
            Sender = new WriteQueue(config); 
        } 

        private static void MessageReceived(object sender, BasicDeliverEventArgs e)
        {
            var s = ReadConfig();
            var body = e.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            Console.WriteLine(DateTime.Now.ToString() + " "+message);
            Listener.Ack(e.DeliveryTag);
            System.Threading.Thread.Sleep(2500);
            Sender.SendMessageToQueue("ping"); 
        }


        public static WrapperScopeSettings ReadConfig()
        {
            IConfigurationBuilder builder = new ConfigurationBuilder()
                                                .SetBasePath(Directory.GetCurrentDirectory())
                                                .AddJsonFile("appsettings.json", optional: false);
            IConfiguration config = builder.Build();
            return config.GetSection("RabbitMQSettings").Get<WrapperScopeSettings>();
        }
    }
}