﻿namespace RabbitMQ.Models
{
    public class WrapperScopeSettings
    { 
        public string ExchangeUri { get; set; } 
        public string ExchangeName { get; set; } 
        public string QueueListenName { get; set; } 
        public string QueuePublishName { get; set; } 
        public string RoutingKeyRead { get; set; }  
        public string RoutingKeyWrite { get; set; }  
        public string ExchangeType { get; set; }
    }
}
