﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Models;
using System; 

namespace RabbitMQ.Wrapper
{
    public class ListenQueue
    { 
        private IConnection connection { get; set; }
        private IModel channel { get; set; }
        private EventingBasicConsumer consumer { get; set; } 
        public ListenQueue(WrapperScopeSettings settings, EventHandler<BasicDeliverEventArgs> MessageReceived) 
        {  
            var factory = new ConnectionFactory() { Uri = new Uri(settings.ExchangeUri) };
            connection = factory.CreateConnection();
            channel = connection.CreateModel();
            channel.ExchangeDeclare(settings.ExchangeName, settings.ExchangeType);
            channel.QueueDeclare(queue: settings.QueueListenName, durable: true, exclusive: false, autoDelete: false);
            channel.QueueBind(settings.QueueListenName, settings.ExchangeName, settings.RoutingKeyRead);

            consumer = new EventingBasicConsumer(channel);
            consumer.Received += MessageReceived;
            channel.BasicConsume(queue: settings.QueueListenName, autoAck: false, consumer: consumer);
        }
        public void Ack(ulong tag) 
        { 
            channel.BasicAck(tag,multiple:false); 
        }
        public void Dispose() 
        {
            connection?.Dispose();
            channel?.Dispose();
        }
    }
}
