﻿using System;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Models;

namespace RabbitMQ.Wrapper
{
    public class WriteQueue
    {
        WrapperScopeSettings _settings { get; set; }
        public WriteQueue(WrapperScopeSettings settings)
        {
            _settings = settings;
        }
        public bool SendMessageToQueue(string message)
        {
            try
            {
                var factory = new ConnectionFactory() { Uri = new Uri(_settings.ExchangeUri) };
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    channel.ExchangeDeclare(_settings.ExchangeName, _settings.ExchangeType);
                    channel.QueueDeclare(queue: _settings.QueuePublishName, durable: true, exclusive: false, autoDelete: false);
                    channel.QueueBind(_settings.QueuePublishName, _settings.ExchangeName, _settings.RoutingKeyWrite);

                    var body = Encoding.UTF8.GetBytes(message);
                    channel.BasicPublish(
                        exchange: _settings.ExchangeName,
                        routingKey: _settings.RoutingKeyWrite,
                        basicProperties: null,
                        body: body);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
